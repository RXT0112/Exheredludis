#!/bin/sh
# shellcheck shell=sh # Written to comply with POXIS IEEE 1003.1-2017

eerror() { funcname="eerror"
	: "Output function used to output error message designed to inform the end-user about non-fatal errors

	SYNOPSIS: ${FUNCNAME:-"$funcname"} [message]

	The format string can be customized using apropriate variable export

	returns fatal error if [message] is not provided

	Written to comply with POXIS IEEE 1003.1-2017
	Linted using shellcheck 0.7.1 (21.10.2020-EU)
	Created by Jacob Hrbek identified using a GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://openpgp.org> in 21/10/2020-EU 16:11:03 CEST under the Zernit License <https://git.dotya.ml/RXT0112/Zernit/LICENSE.md>"

	message="$1"

	# Process the $message
	if [ -n "$message" ]; then
		# FIXME-TRANSLATE
		case "$LANG" in
			en-*|*) ${PRINTF:-printf} "${EERROR_FORMAT_STRING:-"ERROR: %s\\n"}" "$message"
		esac
	elif [ -z "$message" ]; then
		# FIXME-TRANSLATE
		case "$LANG" in
			en-*|*) ${PRINTF:-printf} "${EERROR_FORMAT_STRING_SYNERR:-"SYNTAX-ERROR(${FUNCNAME:-$funcname}): %s\\n"}" "Expected message was not provided in function '${FUNCNAME:-"$funcname"}'"
		esac

		# Terminate the script
		${EXIT:-exit} "${EERROR_EXIT_CODE_SYNERR:-111}"
	else
		die integrity
		exit 255
	fi
}