#!/bin/sh
# shellcheck shell=sh # Written to comply with POXIS IEEE 1003.1-2017

edebug() { funcname="edebug"
	: "Output function used to output debug messaged depending on value stored in 'PALUDIS_DEBUG' variable

	SYNOPSIS: ${FUNCNAME:-"$funcname"} [level] [message]
	
	Written to comply with POXIS IEEE 1003.1-2017
	Linted using shellcheck 0.7.1 (21.10.2020-EU)
	Created by Jacob Hrbek identified using a GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://openpgp.org> in 21/10/2020-EU 16:11:03 CEST under the Zernit License <https://git.dotya.ml/RXT0112/Zernit/LICENSE.md>"

	level="$1"
	message="$2"

	if [ -z "$PALUDIS_DEBUG" ]; then
		return 0
	elif [ -n "$PALUDIS_DEBUG" ]; then
		# DNM-STUB(Krey): Needs to be brainstormed for implementation
		${PRINTF:-printf} 'DEBUG: %s\n' "$message"
	else
		die integrity
	fi
}