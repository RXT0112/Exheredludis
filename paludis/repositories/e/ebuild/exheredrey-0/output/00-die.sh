#!/bin/sh
# shellcheck shell=sh # Written to comply with POXIS IEEE 1003.1-2017

die() { funcname="die"
	: "Output function used to terminate the script
	SYNOPSIS: ${FUNCNAME:-"$funcname"} [exitcode] (message)

	To ensure cross-platform compatibility we are using a string for exitcode that is translated in apropriate integer per expected platform
	- true  ~ translates to ERRCODE 0 for nonfatal assertion
	- false ~ translates to ERRCODE 1 for generic fatal assertion

	The format strings can be customized using apropriate variable exports

	Written to comply with POXIS IEEE 1003.1-2017
	Linted using shellcheck 0.7.1 (21.10.2020-EU)
	Created by Jacob Hrbek identified using a GPG identifier assigned to the electronic mail <kreyren@rixotstudio.cz> according to the keyserver <https://openpgp.org> in 21/10/2020-EU 16:11:03 CEST under the Zernit License <https://git.dotya.ml/RXT0112/Zernit/LICENSE.md>"

	exitcode="$1"
	message="$2"

	case "$exitcode" in
		true)
			# FIXME-TRANSLATE
			case "$LANG" in
				en-*|*) ${PRINTF:-printf} "${DIE_FORMAT_STRING_TRUE:-SUCCESS: %s\\n}" "${message:-Script exitted succesfully}" 
				${EXIT:-exit} "${DIE_TRUE_EXIT_CODE:-0}"
			esac ;;
		false)
			# FIXME-TRANSLATE
			case "$LANG" in
				en-*|*) ${PRINTF:-printf} "${DIE_FORMAT_STRING_FALSE:-FATAL: %s\\n}" "${message:-Script returned a fatal error}" 
				${EXIT:-exit} "${DIE_FALSE_EXIT_CODE:-1}"
			esac ;;
		*)
			# FIXME-TRANSLATE
			case "$LANG" in
				en-*|*) ${PRINTF:-printf} "${DIE_FORMAT_STRING_BUG:-BUG: %s\\n}" "${message:-"Unsupported argument '$exitcode' has been parsed in function '${FUNCNAME:-"$funcname"}}'"}" 
			esac
	esac
}